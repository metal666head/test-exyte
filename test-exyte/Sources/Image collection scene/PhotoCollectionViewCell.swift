//
//  PhotoCollectionViewCell.swift
//  test-exyte
//
//  Created by Anton Marunko on 23/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
}
