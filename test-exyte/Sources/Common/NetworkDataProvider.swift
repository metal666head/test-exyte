//
//  NetworkDataProvider.swift
//  test-exyte
//
//  Created by Anton Marunko on 23/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import Moya
import Gloss

protocol NetworkDataService {
    func getPhotos(completion: @escaping (_ result: [FlickrPhoto])->())
}

class NetworkDataProvider: NetworkDataService {
    
    static let shared = NetworkDataProvider()
    
    func requestPhotos(completion: @escaping (_ result: [FlickrPhoto])->()) {
        let provider = MoyaProvider<NetworkService>()
        
        provider.request(.photosList) { [unowned self] result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                
                if let photos = self.parseResponse(data) {
                    completion(photos)
                }
                
            case let .failure(error):
                AlertManager.shared.showAlert(message: error.errorDescription ?? NSLocalizedString("Error", comment: ""))
            }
        }
    }
    
    func parseResponse(_ data: Data) -> [FlickrPhoto]? {
        
        if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
            guard let dictionary = json as? [String: Any] else {
                return nil
            }
            
            if let photosObject = dictionary["photos"] as? [String: Any] {
                guard let photos = [FlickrPhoto].from(jsonArray: photosObject["photo"] as! [JSON]) else {
                    return nil
                }
                
                return photos
            }
        }
        
        return nil
    }
    
    public func getPhotos(completion: @escaping (_ result: [FlickrPhoto])->()) {
        requestPhotos { photos in
            completion(photos)
        }
    }
}
