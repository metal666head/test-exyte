//
//  String+extensions.swift
//  test-exyte
//
//  Created by Anton Marunko on 21/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation

extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
