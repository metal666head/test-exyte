//
//  NetworkService.swift
//  test-exyte
//
//  Created by Anton Marunko on 21/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import Moya

fileprivate let flickrKey = "7212b6b5d37ab38cc86c7fd30d37bed5"
fileprivate let flickrSecret = "ea9f691ad021a7ef"
fileprivate let groupId = "10269355@N00"

fileprivate let groupPhotosMethod = "flickr.groups.pools.getPhotos"

fileprivate let jsonFormat = "json"
enum NetworkService {
    case photosList
}

extension NetworkService: TargetType {
    
    var baseURL: URL { return URL(string: "https://api.flickr.com/services/rest/")! }
    
    var path: String {
        switch self {
        case .photosList:
            return ""
        }
    }
        
    var method: Moya.Method {
        switch self {
        case .photosList:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .photosList:
            return ["method": groupPhotosMethod,
                    "api_key": flickrKey,
                    "group_id": groupId,
                    "format": jsonFormat,
                    "nojsoncallback": 1]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .photosList:
            return URLEncoding.queryString
        }
    }
        
    var task: Task {
        switch self {
        case .photosList:
            return .request
        }
    }
    
    var sampleData: Data {
        switch self {
        case .photosList:
            return Data.init()
        }
    }
}
