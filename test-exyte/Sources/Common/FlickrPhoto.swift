//
//  FlickrPhoto.swift
//  test-exyte
//
//  Created by Anton Marunko on 21/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import Gloss

final class FlickrPhoto: NSObject, Decodable {
    
    var photoId: String?
    var farm: Int?
    var secret: String?
    var server: String?
    var title: String?
    
    var photoUrl: NSURL {
        return NSURL(string: "https://farm\(farm ?? 0).staticflickr.com/\(server ?? "0")/\(photoId ?? "0")_\(secret ?? "").jpg")!
    }
    
    init?(json: JSON) {
        
        self.photoId = "id" <~~ json
        self.farm = "farm" <~~ json
        self.secret = "secret" <~~ json
        self.server = "server" <~~ json
        self.title = "title" <~~ json
    }
    
    override init() {
        super.init()
    }
}

extension FlickrPhoto: RealmConvertable {

    static func mapFromRealmObject(_ object: PhotoData) -> FlickrPhoto {
        let model = FlickrPhoto()
        
        model.photoId = object.photoId
        model.farm = object.farm
        model.secret = object.secret
        model.server = object.server
        model.title = object.title
        
        return model
    }
    
    func mapToRealmObject() -> PhotoData {
        let model = PhotoData()
        
        model.photoId = photoId ?? ""
        model.farm = farm ?? 0
        model.secret = secret ?? ""
        model.server = server ?? ""
        model.title = title ?? ""
        
        return model
    }
}
