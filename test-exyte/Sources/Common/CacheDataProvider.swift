//
//  DataProvider.swift
//  test-exyte
//
//  Created by Anton Marunko on 23/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import RealmSwift

protocol CacheData {
    func getPhotosFromCache() -> Results<PhotoData>
    func addPhotosToCache(photos: [FlickrPhoto])
}

class CacheDataProvider: DataService, UpdateCacheService {
    
    let cache = RealmCache()

    func getPhotos()->Results<PhotoData> {
        return cache.getPhotosFromCache()
    }
    
    func addPhotosToCache(photos: [FlickrPhoto]) {
        cache.addPhotosToCache(photos: photos)
    }
}
