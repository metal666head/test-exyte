//
//  File.swift
//  test-exyte
//
//  Created by Anton Marunko on 24/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import INSPhotoGallery
import Kingfisher

class PhotoViewable: NSObject, INSPhotoViewable, Resource {
    func loadImageWithCompletionHandler(_ completion: @escaping (UIImage?, Error?) -> ()) {
        if imageURL != nil {
            
            KingfisherManager.shared.retrieveImage(with: self, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
                completion(image, error)
            })
        } else {
            completion(nil, NSError(domain: "PhotoDomain", code: -1, userInfo: [ NSLocalizedDescriptionKey: "Couldn't load image"]))
        }
    }
    
    func loadThumbnailImageWithCompletionHandler(_ completion: @escaping (_ image: UIImage?, _ error: Error?) -> ()) {
        completion(nil, nil)
    }
    

    var photoId: String?
    var image: UIImage?
    var thumbnailImage: UIImage?
    
    var cacheKey: String { return photoId! }
    var downloadURL: URL { return imageURL! as URL }
    
    var imageURL: NSURL?
    var thumbnailImageURL: NSURL?
    var attributedTitle: NSAttributedString? 
    
    init(photoData: FlickrPhoto) {
        imageURL = photoData.photoUrl
        photoId = photoData.photoId
    }

}
