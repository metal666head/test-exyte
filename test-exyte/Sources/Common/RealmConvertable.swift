//
//  File.swift
//  test-exyte
//
//  Created by Anton Marunko on 24/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmConvertable {
    
    associatedtype RealmType: Object
    
    func mapToRealmObject() -> RealmType
    static func mapFromRealmObject(_ object: RealmType) -> Self
}
