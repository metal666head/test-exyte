//
//  RealmCache.swift
//  test-exyte
//
//  Created by Anton Marunko on 23/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import RealmSwift

class RealmCache: CacheData {
    var realm = try! Realm()
    
    func getPhotosFromCache() -> Results<PhotoData> {
        return self.realm.objects(PhotoData.self)
    }
    
    func addPhotosToCache(photos: [FlickrPhoto]) {
        try! realm.write {
            realm.add(photos.map({ $0.mapToRealmObject() }), update: true)
        }
    }
}
