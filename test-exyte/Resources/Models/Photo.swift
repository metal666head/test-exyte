//
//  Photo.swift
//  test-exyte
//
//  Created by Anton Marunko on 23/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import RealmSwift

class PhotoData: Object {
    
    dynamic var photoId: String = ""
    dynamic var farm: Int = 0
    dynamic var secret: String = ""
    dynamic var server: String = ""
    dynamic var title: String = ""
    
    override static func primaryKey() -> String? {
        return "photoId"
    }
}
